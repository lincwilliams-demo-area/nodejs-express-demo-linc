### Node Express template project with tests and GitLab Pages

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).  Please read the about the test stage below!

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

The test stage does not use the GitLab test template!  This is becuase the test template only runs Herokuish image.  This project installs NPM, Mocha, and Chai for the test job to execute properly.  [Mocha](https://mochajs.org/) is a javascript test framework for node js.  [mocha-junit-reporter](https://www.npmjs.com/package/mocha-junit-reporter) allows for the creation of the junit xml report which is required by the GitLab Test widget to disply test results.  [mocha-simple-html-reporter](https://www.npmjs.com/package/mocha-junit-reporter) generates an html junit report whcih is loaded to pages to display a nicely formated test report. [Chai](https://www.chaijs.com/) Chai is a BDD / TDD assertion library for node and the browser that can be paired with any javascript testing framework.

Here is the link to the [GitLab Pages Website](https://sa_group_pfd.gitlab.io/nodejs_express_demo/)

For Pages to work properly, the html report file that is generated, must be moved to the /public folder of pages and renamed as index.html.  Knowing where the files that you are pushing to pages is critical so that they can be moved to the public directory or appropriate sub-folder.
